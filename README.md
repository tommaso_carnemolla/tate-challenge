﻿# Tate Challenge

La soluzione è stata sviluppata in Node JS e suddivisa in due funzioni serverless:
la prima funzione, triggerata con eventi schedulati, si occupa di verificare la necessità di effettuare o meno il download del file clienti ed eventualmente effettua l'operazione.
La seconda funzione è rappresentata da un server graphql con un endpoint dedicato all'interrogazion dei dati dei clienti.
Per la persistenza è sato scelto Atlas, servizio cloud di MongoDb.


## Installazione serverless framework via npm:

Lanciare il seguente comando

`npm install -g serverless`

## Configurazione delle proprie credenziali AWS

[Provider Credentials
](https://github.com/serverless/serverless/blob/master/docs/providers/aws/guide/credentials.md)

## Installazione le dipendenze

Lanciare il comando npm di installazione delle dipendenze 

```
# nella root e nelle directory /tate-challenge-api e
# /tate-challenge-customer-fetcher
 npm install
```

## Deploy su AWS Lamda

Lanciare il seguente comando per avviare il deploy
  ```
  npm run-script deploy
  # utlizzare la seguente variante per non eseguire i test
  npm run-script deploy:skiptest
  ```

## Configurazione

è possibile modificare le seguenti variabili d'ambientenel file .env:
  
  ```
  # nome DB
  DB_NAME=tate_customers
  # credenziali connessione a Mongo Atlas
  DB_USER=tateDbAdmin
  DB_PASSWORD=9saFIXyxLOKonuXW
  # nome cluster su Mongo Atlas
  DB_CLUSTER=tatechallenge-jcd5j.mongodb.net
  # Indirizzo del servizio per il download del file clienti
  API_URL=http://challenges.tate.cloud/back2018/CLIENTI
  # validità (in secondi) della chache lato server
  CACHE_MAX_AGE=1800
  ```


