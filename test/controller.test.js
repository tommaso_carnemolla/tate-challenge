const controller = require('../tate-challenge-customer-fetcher/src/controller')

describe('download flow', () => {
    it('download csv', async () => {
        expect.assertions(1);
        let customers = await controller.getCustomers();
        expect(true).toBe(true);
    })
    it('should start the download flow if check status return true', async () => {
        controller.checkEventStatus = jest.fn(() => true);
        controller.getCustomers = jest.fn(() => []);
        controller.registerEvent = jest.fn();
        controller.insertOrUpdateCustomers = jest.fn();

        expect.assertions(1);
        await controller.downloadCustomersCsv();
        expect(controller.getCustomers).toBeCalledTimes(1);
    })
    it('should not start the download flow if check status return false', async () => {

        controller.checkEventStatus = jest.fn(() => false);
        controller.getCustomers = jest.fn(() => []);
        controller.registerEvent = jest.fn();
        controller.insertOrUpdateCustomers = jest.fn();

        expect.assertions(1);
        await controller.downloadCustomersCsv();
        expect(controller.getCustomers).not.toBeCalled();
    })
})

describe('register event', () => {
    it('should register an event with records', async () => {
        controller.checkEventStatus = jest.fn(async () => true);
        controller.getCustomers = jest.fn(async () => [{ idUtente: '1' }, { idUtente: '2' }]);
        controller.registerEvent = jest.fn();
        controller.insertOrUpdateCustomers = jest.fn();
        expect.assertions(1);
        await controller.downloadCustomersCsv();
        expect(controller.registerEvent).toBeCalledWith(2);
    })
    it('should register an event with zero records if no customer is returned', async () => {
        controller.checkEventStatus = jest.fn(async () => true);
        controller.getCustomers = jest.fn(async () => []);
        controller.registerEvent = jest.fn();
        controller.insertOrUpdateCustomers = jest.fn();
        expect.assertions(1);
        await controller.downloadCustomersCsv();
        expect(controller.registerEvent).toBeCalledWith(0);
    })
    it('should register an event with error if error is returned', async () => {
        let errorMock = new Error('test error');
        controller.checkEventStatus = jest.fn(async () => true);
        controller.getCustomers = jest.fn();
        controller.getCustomers.mockImplementation(async () => {
            throw errorMock;
        });
        controller.registerEvent = jest.fn();
        controller.insertOrUpdateCustomers = jest.fn();
        expect.assertions(1);
        try {
            await controller.downloadCustomersCsv();

        } catch (error) {
            expect(controller.registerEvent).toBeCalledWith(0, errorMock.message);
        }
    })
})