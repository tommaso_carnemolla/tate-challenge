const MongoClient = require("mongodb").MongoClient;
const eventRepository = require("../common/repositories/eventRepository");

let connection;
let db;
let events;


let mockEvent = {
    day: new Date(),
    recordsNumber: 30,
    error: null
};

beforeAll(async () => {
    connection = await MongoClient.connect(global.__MONGO_URI__, {
        useNewUrlParser: true
    });
    db = await connection.db(global.__MONGO_DB_NAME__);
    await eventRepository.injectDB(db);
    events = db.collection("events");

    mockEvent.day.setUTCHours(0,0,0,0);

});

afterAll(async () => {
    await connection.close();
});
beforeEach(async () => {
    await events.deleteMany({});
});
describe("insert or update event", () => {
    it("should insert an event into collection", async () => {

        const insertResult = await eventRepository.insertOrUpdateEvent(mockEvent);

        expect(insertResult.upsertedCount).toBe(1);
        expect(insertResult.upsertedId).not.toBe(null);
        expect(insertResult.modifiedCount).toBe(0);
    });

    it("should update an existing event", async () => {


        await events.insertOne(mockEvent);

        mockEvent.recordsNumber = 100;

        const updateResult = await eventRepository.insertOrUpdateEvent(mockEvent);

        expect(updateResult.upsertedCount).toBe(0);
        expect(updateResult.modifiedCount).toBe(1);

        const updatedEvent = await events.findOne({ day: mockEvent.day });

        expect(updatedEvent.recordsNumber).toBe(100);
    });
});

describe('retrieve events', () => {

    let insertedEvent;

    beforeEach( async () => {
       insertedEvent = await events.insertOne(mockEvent);
    })

    it('should retrieve an event by day', async () => {
        retrievedEvent = await eventRepository.getEventByDay(mockEvent.day);
        expect(retrievedEvent._id).toEqual(insertedEvent.insertedId);
    })

})

