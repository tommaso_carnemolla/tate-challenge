const MongoClient = require("mongodb").MongoClient;
const customerRepository = require("../common/repositories/customerRepository");
const testUtils = require('../common/utils/testUtils');
const repositoryUtils = require('../common/utils/repositoryUtils');

let connection;
let db;
let customers;
let customersArr = [];
let customersToInsert = [];

  

beforeAll(async () => {
  connection = await MongoClient.connect(global.__MONGO_URI__, {
    useNewUrlParser: true
  });
  db = await connection.db(global.__MONGO_DB_NAME__);
  await customerRepository.injectDB(db);
  customers = db.collection("customers");

});

afterAll(async () => {
  await connection.close();
});

describe("insert customer", () => {

  beforeEach(async () => {
    await customers.deleteMany({});
    customersArr = testUtils.generateMockCustomers(2);

    customersToInsert = customersArr.map(customer => {
      let idUtente;
      let customerWoID;
      ({ idUtente, ...customerWoID } = customer);
      return { _id: idUtente, ...customerWoID };
    })
  });

  it("should insert a customer into collection", async () => {

    await customerRepository.insertOrUpdateCustomer(customersArr[0]);

    const insertedCustomer = await customers.aggregate(repositoryUtils.createAggregationPipeline({idUtente: "1"}, null, null, null)).next();

    expect(insertedCustomer).toEqual(customersArr[0]);
  });
  
  it("should update an existing customer", async () => {    

    
    await customers.insertOne(customersToInsert[0]);

    customersArr[0].nome = 'modified';

    let updateResult = await customerRepository.insertOrUpdateCustomer(customersArr[0]);

    expect(updateResult.upsertedCount).toBe(0);
    expect(updateResult.modifiedCount).toBe(1);

    const updatedCustomer = await customers.aggregate(repositoryUtils.createAggregationPipeline({idUtente: "1"}, null, null, null)).next();

    expect(updatedCustomer.nome).toBe('modified');
  });

  it("should bulk update or insert customers", async () => {    

    await customers.insertOne(customersToInsert[0]);

    customersArr[0].nome = 'modified';

    let bulkResult = await customerRepository.bulkInsertOrUpdateCustomers(customersArr);

    expect(bulkResult.modifiedCount).toBe(1);
    expect(bulkResult.upsertedCount).toBe(1);

    const updatedCustomer = await customers.aggregate(repositoryUtils.createAggregationPipeline({idUtente: "1"}, null, null, null)).next();
    const insertedCustomer = await customers.aggregate(repositoryUtils.createAggregationPipeline({idUtente: "2"}, null, null, null)).next();


    expect(updatedCustomer.nome).toBe('modified');
    expect(insertedCustomer).toEqual(customersArr[1]);
  });
});

describe('retrieve customers', () => {

  beforeAll(async () => {

    await customers.deleteMany({});
    
    customersArr = testUtils.generateMockCustomers(30);

    customersToInsert = customersArr.map(customer => {
        let idUtente;
        let customerWoID;
        ({ idUtente, ...customerWoID } = customer);
        return {_id: idUtente, ...customerWoID};
    })
    
    await customers.insertMany(customersToInsert);
});

  it('should retrieve customer filtering by name', async () => {
      expect.assertions(1);

      searchCustomer = customersArr[0];

      const foundCustomer = await customerRepository.getCustomersByFilter({nome: searchCustomer.nome}, null, null, null);

      expect(foundCustomer[0]).toEqual(searchCustomer);
  });
  it('should not retrieve customer filtering by name if not present', async () => {
      expect.assertions(1);

      const foundCustomer = await customerRepository.getCustomersByFilter({nome: "A_AAA-AA"}, null, null, null);

      expect(foundCustomer.length).toBe(0);
  });
  it('should retrieve customer filtering by surname and status', async () => {
      expect.assertions(1);

      searchCustomer = customersArr[0];

      const foundCustomer = await customerRepository.getCustomersByFilter({cognome: searchCustomer.cognome, stato: searchCustomer.stato}, null, null, null);

      expect(foundCustomer[0]).toEqual(searchCustomer);
  });
  it('should retrieve customers third page', async () => {
      expect.assertions(3);
      const pageSize = 5;
      const page = 3;

      const foundCustomers = await customerRepository.getCustomersByFilter(null, (page-1)*pageSize, pageSize, null);

      expect(foundCustomers.length).toEqual(5);
      expect(foundCustomers[0]).toEqual(customersArr.slice((page-1)*pageSize, (3*pageSize)+1)[0]);
      expect(foundCustomers[pageSize-1]).toEqual(customersArr.slice((page-1)*pageSize, (page*pageSize)+1)[pageSize-1]);
  });
  it('should retrieve customers ordered by name ascending', async () => {
      expect.assertions(3);

      const foundCustomers = await customerRepository.getCustomersByFilter(null, null, null,{nome: "asc"});

      const orderedCustomers = customersArr.sort((a, b) =>{
          if(a.nome < b.nome) { return -1; }
          if(a.nome > b.nome) { return 1; }
          return 0;
      });

      expect(foundCustomers[0]).toEqual(orderedCustomers[0]);
      expect(foundCustomers[foundCustomers.length/2]).toEqual(orderedCustomers[foundCustomers.length/2]);
      expect(foundCustomers[foundCustomers.length-1]).toEqual(orderedCustomers[foundCustomers.length-1]);

  });
  it('should retrieve customers ordered by name descending', async () => {
      expect.assertions(3);
      

      const foundCustomers = await customerRepository.getCustomersByFilter(null, null, null,{nome: "desc"});

      const orderedCustomers = customersArr.sort((a, b) =>{
          if(a.nome < b.nome) { return 1; }
          if(a.nome > b.nome) { return -1; }
          return 0;
      });

      expect(foundCustomers[0]).toEqual(orderedCustomers[0]);
      expect(foundCustomers[foundCustomers.length/2]).toEqual(orderedCustomers[foundCustomers.length/2]);
      expect(foundCustomers[foundCustomers.length-1]).toEqual(orderedCustomers[foundCustomers.length-1]);

  });
})
