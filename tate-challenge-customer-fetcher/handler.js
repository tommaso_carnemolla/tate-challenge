const dbClient = require('../common/dbClient');
const customerRepository = require('../common/repositories/customerRepository');
const eventRepository = require('../common/repositories/eventRepository');
const controller = require('./src/controller');

const initDB = async () => {
  let db = await dbClient.dbConnect();
  await customerRepository.injectDB(db);
  await eventRepository.injectDB(db);
};

module.exports.customerFetcher = async (
  event,
  context
) => {
  context.callbackWaitsForEmptyEventLoop = false;

  console.log(`event called at: ${event.time} `);

  try {
    await initDB();
    console.log('connected to MongoDB');
    const handlerResult = await controller.downloadCustomersCsv();
    console.log(handlerResult);

    return handlerResult;    
    
  } catch (error) {
    console.error(error);
    throw error;
  }
};