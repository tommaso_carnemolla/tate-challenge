const axios = require('axios');

module.exports = class HttpClient {
    static async getCsv() {
        try {
            const url = process.env.API_URL;

            return await axios({
                url,
                method: 'GET',
                responseType: 'stream'
            });

        } catch (error) {
            throw Error(`Errore nel download del csv: ${error}`);;
        }
    }
}