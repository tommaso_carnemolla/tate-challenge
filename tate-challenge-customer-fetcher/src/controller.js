const httpClient = require('./httpClient');
const parse = require('csv-parse');
const camelCase = require('camelcase')
const customerRepository = require('../../common/repositories/customerRepository')
const eventRepository = require('../../common/repositories/eventRepository')

const columnsFun = (header) => header.map(column => camelCase(column));

const parser = parse({
    delimiter: ';',
    columns: columnsFun
});




module.exports = class Controller {

    static async getCustomers() {
        const customers = [];
        try {
            let response = await httpClient.getCsv();

            response.data.pipe(parser);

            parser.on('readable', () => {
                let record;
                while (record = parser.read()) {
                    customers.push(record);
                }
            });

            return new Promise((resolve, reject) => {
                parser.on('finish', () => resolve(customers))
                parser.on('error', () => reject('errore nel parsing del file csv'))
            });
        } catch (error) {
            throw Error(`Errore nel parsing del csv: ${error}`);
        }
    }

    static async registerEvent(numRecords, error = null) {
        let day = new Date();
        day.setUTCHours(0,0,0,0);
    

        try {
            return await eventRepository.insertOrUpdateEvent({day: day, numRecords: numRecords, error: null});
            
        } catch (error) {
            throw Error(`Errore nell'inserimento del nuovo evento: ${error}`);;
        }


    }
    
    static async insertOrUpdateCustomers(customers) {

        try {
            return await customerRepository.bulkInsertOrUpdateCustomers(customers);
            
        } catch (error) {
            throw Error(`Errore nell'inserimento massivo dei customers: ${error}`);;
        }

    
    }

    static async checkEventStatus() {
        let day = new Date();
        day.setUTCHours(0,0,0,0);
        try {
            let event = await eventRepository.getEventByDay(day);
            return !event || event.numRecords == 0 || event.error;
            
        } catch (error) {
            throw Error(`Errore nel check degli eventi: ${error}`);;
        }
    }

    static async downloadCustomersCsv() {

        let opResult = null;
        try {


            let proceed = await this.checkEventStatus();

            if(!proceed) return {
                result: `today customers already inserted`
            };

            const customers = await this.getCustomers();

            await this.registerEvent(customers.length)

            if(customers.length >0) {

                opResult = await this.insertOrUpdateCustomers(customers);
            }


            return {
                result: `inserted or updated ${opResult? (opResult.upsertedCount + opResult.modifiedCount) : 0} records`
            };


        } catch (error) {
            await this.registerEvent(0, error.message);
            throw Error(`Errore nel processo di download del csv: ${error}`);;
        }

    }
}