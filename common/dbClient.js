const MongoClient = require('mongodb').MongoClient;

let cachedDb = null;

async function connect() {
    try {
        if (!cachedDb || !cachedDb.serverConfig.isConnected()) {
            console.log('new connection');
            let client = await MongoClient.connect(`mongodb+srv://${process.env.DB_USER}:${
                process.env.DB_PASSWORD
                }@${process.env.DB_CLUSTER}/${process.env.DB_NAME}`, { useNewUrlParser: true });
            cachedDb = client.db(process.env.DB_NAME);
        }
        return cachedDb;
    } catch (error) {
        throw error;
    }
}

module.exports = class DbClient {

    static async dbConnect(){
        try {
            return await connect();
            
        } catch (error) {
            throw error;
        }
    }

}