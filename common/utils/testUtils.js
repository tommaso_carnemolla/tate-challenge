const faker = require('faker');

module.exports = class TestUtils {

    static generateMockCustomers(num) {

        let customers = [];

        for (let index = 1; index <= num; index++) {
            customers.push({
                idUtente: index.toString(),
                codiceCliente: index.toString(),
                email: faker.internet.email(),
                nome: faker.name.firstName(),
                cognome: faker.name.lastName(),
                dataNascita: "01_01_1988",
                codiceFiscale: "PRTGVR63P64L91",
                stato: faker.random.boolean() ? "ATTIVAZIONE" : "STANDBY",
                attivo: faker.random.boolean() ? "SI" : "NO",
                nuovo: faker.random.boolean() ? "SI" : "NO",
                rete: faker.random.boolean() ? "App" : "Tate Assistenza",
                agente: faker.name.findName(),
                canoneRai: faker.random.boolean() ? "SI" : "NO"
            });
        }

        return customers;
    }

}