module.exports = class RepositoryUtils {
    static createAggregationPipeline(match, skip, limit, sort) {
        let pipeline = [];
    
        if (match) {
            if(match.idUtente){
                match._id = match.idUtente;
                delete match.idUtente;
            }
            pipeline.push({
                $match: match
            });
        }

        if (sort) {
            let sortObj = {};
            let sortKeys = Object.keys(sort);
            sortKeys.forEach(k => {
                sortObj[k] = sort[k] == 'asc' ? 1 : -1;
            })
            pipeline.push({ 
                $sort : sortObj 
            });
        }
    
        if (skip) {
            pipeline.push({
                $skip: skip
            });
        }
        if (limit) {
            pipeline.push({
                $limit: limit
            });
        }
    
        pipeline.push({
            $addFields: {
                idUtente: "$_id"
            }
        });
    
        pipeline.push({
            $project: {
                _id: 0
            }
        });
    

        return pipeline;
        
    }
}