const repositoryUtils = require('../utils/repositoryUtils');

let customers;

module.exports = class CustomerRepository {

    static async injectDB(db) {
        if (customers) {
            return
        }
        try {
            customers = await db.collection("customers")
            this.customers = customers;
        } catch (e) {
            throw Error(`impossibile stabilire una connessione per gestire la collection: ${e}`)
        }
    }

    static async insertOrUpdateCustomer(customer) {
        let idUtente;
        let customerWoID;
        ({ idUtente, ...customerWoID } = customer);
        try {
            return await customers.updateOne({ _id: idUtente }, { $set: customerWoID }, {
                upsert: true
            });
            
        } catch (error) {
            throw error;
        }
    }

    static async bulkInsertOrUpdateCustomers(customersArr) {

        const customersToInsert = customersArr.map(customer => {
            let idUtente;
            let customerWoID;
            return ({ idUtente, ...customerWoID } = customer);
        })

        const updateOperations = customersToInsert.map((element) => {
            const { idUtente, ...customerWoID } = element;
            return {
                updateOne: {
                    filter: { _id: idUtente },
                    update: { $set: customerWoID },
                    upsert: true
                }
            }
        });

        try {
            
            return await customers.bulkWrite(updateOperations);
        } catch (error) {
            throw error;
        }
    }

    static async getCustomersByFilter(filter, skip, limit, sort) {
        try {
            return await customers.aggregate(repositoryUtils.createAggregationPipeline(filter, skip, limit, sort)).toArray();
        } catch (error) {
            throw Error(`Errore nel recupero della lista customers: ${error}`);
        }
    }
}