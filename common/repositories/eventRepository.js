let events;

module.exports = class EventRepository {

    static async injectDB(db) {
        if (events) {
            return
        }
        try {
            events = await db.collection("events")
            this.events = events;
        } catch (e) {
           throw Error(`impossibile stabilire una connessione per gestire la collection: ${e}`)
        }
    }

    static async insertOrUpdateEvent(event) {
        try {
            
            return await events.updateOne({ day: event.day }, { $set: event }, {
                upsert: true
            });
        } catch (error) {
            throw error;
        }
    }

    static async getEventByDay(day) {
        try {
            
            return events.findOne({day: day});
        } catch (error) {
            throw error;
        }
    }
}