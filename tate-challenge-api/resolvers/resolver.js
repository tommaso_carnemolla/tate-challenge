const customerRepository = require('../../common/repositories/customerRepository');
const NodeCache = require('node-cache');
const customersCache = new NodeCache();

const getCachedResult = (filter, size, page, sort) => {
    const cachedResult = customersCache.get(JSON.stringify({ filter, size, page, sort }));
    if (cachedResult) {
      return cachedResult;
    }
    return null;
  }

  const getCustomersByFilter = (filter, size, page, sort) => {

    let searchResult = customerRepository.getCustomersByFilter(filter, (page - 1) * size, size, sort);

    searchResult.then(result => {
      customersCache.set(JSON.stringify({ filter, size, page, sort }), result, process.env.CACHE_MAX_AGE);
    })

    return searchResult;
  }

  const customersResolver = (parent, { filter, size, page, sort }, context, info) => {
    try {
      const cachedResult = getCachedResult(filter, size, page, sort);

      if (cachedResult) {
        return cachedResult
      }

      return getCustomersByFilter(filter, size, page, sort);

    } catch (error) {
      throw (`errore nella funzione di risoluzione: ${error}`);
    }
  };


const resolvers = {
  Query: {
    customers: customersResolver
  },
};


module.exports = resolvers;