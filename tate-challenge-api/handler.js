const { ApolloServer } = require('apollo-server-lambda');
const typeDefs = require('./types/customer');
const resolvers = require('./resolvers/resolver');
const dbClient = require('../common/dbClient');
const customerRepository = require('../common/repositories/customerRepository');


const initDB = async () => {
    let db = await dbClient.dbConnect();
    await customerRepository.injectDB(db);
    return customerRepository;
};

const runHandler = (event, context, handler) =>
	new Promise((resolve, reject) => {
		const callback = (error, body) => (error ? reject(error) : resolve(body));

    if (event.httpMethod === 'GET') {
          handler(
            { ...event, path: event.requestContext.path || event.path },
            context,
            callback,
          );
        } else {
          handler(event, context, callback);
        }

	});

const run = async (event, context) => {
  
  await initDB();

	const server = new ApolloServer({
    typeDefs,
    resolvers,
	});
	const handler = server.createHandler();
	const response = await runHandler(event, context, handler);

	return response;
};

module.exports.graphqlServer = run;