const { gql } = require('apollo-server-lambda');

const typeDefs = gql`

type Query {
  customers(filter: CustomerInput, size: Int, page: Int, sort: CustomerInput): [Customer]
}

type Customer {
  idUtente: String
  codiceCliente: String
  email: String
  nome: String
  cognome: String
  dataNascita: String
  codiceFiscale: String
  stato: String
  attivo: String
  nuovo: String
  rete: String
  agente: String
  canoneRai: String
}

input CustomerInput {
  idUtente: String
  codiceCliente: String
  email: String
  nome: String
  cognome: String
  dataNascita: String
  codiceFiscale: String
  stato: String
  attivo: String
  nuovo: String
  rete: String
  agente: String
  canoneRai: String
}



`;

module.exports = typeDefs;